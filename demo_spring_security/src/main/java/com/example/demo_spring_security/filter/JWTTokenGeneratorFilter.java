package com.example.demo_spring_security.filter;

import com.example.demo_spring_security.constant.SecurityConstants;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.crypto.SecretKey;
import java.io.IOException;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class JWTTokenGeneratorFilter extends OncePerRequestFilter {
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if(authentication != null){
            SecretKey key = Keys.hmacShaKeyFor(SecurityConstants.JWT_KEY.getBytes());
            String jwt = Jwts.builder().setIssuer("Bank").setSubject("JWT Token")
                    .claim("username", authentication.getName())
                    .claim("authorities", convertAuthorities(authentication.getAuthorities()))
                    .setIssuedAt(new Date())
                    .setExpiration(new Date(new Date().getTime() + 3000000))
                    .signWith(key).compact();

            response.setHeader(SecurityConstants.JWT_HEADER, jwt);
        }
        System.out.println(authentication + " GENERATOR");
        System.out.println(convertAuthorities(authentication.getAuthorities()) + " GENERATOR CONVERT");
        filterChain.doFilter(request, response);
    }

    public boolean shouldNotFilter(HttpServletRequest request){
        return !request.getServletPath().equals("/user");
    }

    private String convertAuthorities(Collection<? extends GrantedAuthority> collections){
        Set<String> authorities = new HashSet<>();
        for (GrantedAuthority a: collections) {
            authorities.add(a.getAuthority());
        }

        return String.join(",", authorities);
    }
}
