package com.example.demo_spring_security.service.impl;

import com.example.demo_spring_security.entities.Customer;
import com.example.demo_spring_security.entities.UserDetailImpl;
import com.example.demo_spring_security.repositories.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailServiceImpl implements UserDetailsService {

    //User doit implémenter userDetails

    //Provider, generator toker
    //Provider d'authentification => Users details pour l'entity user ( avec les différentes méthodes )

    @Autowired
    private CustomerRepository _customerRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Customer customer = _customerRepository.findByEmailIs(username);
        return UserDetailImpl.build(customer);
    }
}
