package com.example.demo_spring_security.constant;

public interface SecurityConstants {
    public static final String JWT_KEY = "jxgEQeXHuPq8VdbyYFNkANdudQ53YUn4";
    public static final String JWT_HEADER = "Authorization";
}
