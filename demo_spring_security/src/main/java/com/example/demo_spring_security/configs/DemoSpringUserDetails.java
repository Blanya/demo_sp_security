//package com.example.demo_spring_security.configs;
//
//import com.example.demo_spring_security.entities.Customer;
//import com.example.demo_spring_security.repositories.CustomerRepository;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.core.GrantedAuthority;
//import org.springframework.security.core.authority.SimpleGrantedAuthority;
//import org.springframework.security.core.userdetails.User;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.core.userdetails.UsernameNotFoundException;
//import org.springframework.stereotype.Service;
//
//import java.util.ArrayList;
//import java.util.List;
//
//@Service
//public class DemoSpringUserDetails implements UserDetailsService {
//
//    @Autowired
//    private CustomerRepository _customerRepository;
//    @Override
//    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//        String userName, password = null;
//        List<GrantedAuthority> authorities = null;
//        List<Customer> list = _customerRepository.findByEmail(username);
//
//        if(list.size() == 0)
//            throw new UsernameNotFoundException("User not found: " + username);
//        else{
//            userName = list.get(0).getEmail();
//            password = list.get(0).getPwd();
//            authorities = new ArrayList<>();
//            authorities.add(new SimpleGrantedAuthority(list.get(0).getRole()));
//        }
//
//        return new User(userName, password, authorities);
//    }
//}
