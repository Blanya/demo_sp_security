package com.example.demo_spring_security.configs;

import com.example.demo_spring_security.entities.Authority;
import com.example.demo_spring_security.entities.Customer;
import com.example.demo_spring_security.repositories.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
public class DemoUsernamePwdAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    CustomerRepository _customerRepository;
    @Autowired
    PasswordEncoder _passwordEncoder;


//    @Override
//    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
//        String userName, password = null;
//        userName = authentication.getName();
//        password = authentication.getCredentials().toString();
//
//
//        List<Customer> listes = _customerRepository.findByEmail(userName);
//        if(listes.size() > 0){
//            if(_passwordEncoder.matches(password, listes.get(0).getPwd())){
////                List<GrantedAuthority> authorities = new ArrayList<>();
////                authorities.add(new SimpleGrantedAuthority(listes.get(0).getRole()));
//               UsernamePasswordAuthenticationToken token = new  UsernamePasswordAuthenticationToken(userName, password, getGrantedAuthorities(listes.get(0).getAuthorities()));
//
//                System.out.println(token + "UPAP");
//                return token;
//                //passer par une classe car authentication => interface
//            }else
//                throw new BadCredentialsException("Invalid password");
//        }else
//            throw new BadCredentialsException("User not registered");
//    }

    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String username, password = null;
        username = authentication.getName();
        password = authentication.getCredentials().toString();

        Customer customer = _customerRepository.findByEmailIs(username);
        if (_passwordEncoder.matches(password, customer.getPwd())) {
            return new UsernamePasswordAuthenticationToken(username, password, getGrantedAuthorities(customer.getAuthorities()));
        } else {
            throw new BadCredentialsException("Invalid password!");
        }
    }

    private List<GrantedAuthority> getGrantedAuthorities(Set<Authority> authorities){
        List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
        for (Authority a: authorities) {
            grantedAuthorities.add(new SimpleGrantedAuthority(a.getName()));
        }
        System.out.println(grantedAuthorities + " UPAP");
        return grantedAuthorities;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication);
    }
}
