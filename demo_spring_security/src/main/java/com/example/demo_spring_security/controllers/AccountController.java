package com.example.demo_spring_security.controllers;

import com.example.demo_spring_security.entities.Account;
import com.example.demo_spring_security.repositories.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AccountController {

    @Autowired
    private AccountRepository _accountRepository;

    @GetMapping("/myAccount")
    public Account getAccountDetails(@RequestParam int id) {
        Account accounts = _accountRepository.findByCustomerId(id);
        if (accounts != null ) {
            return accounts;
        }else {
            return null;
        }
    }
}
