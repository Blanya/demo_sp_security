package com.example.demo_spring_security.controllers;

import com.example.demo_spring_security.dtos.LoginRequestDto;
import com.example.demo_spring_security.dtos.LoginResponseDto;
import com.example.demo_spring_security.entities.Customer;
import com.example.demo_spring_security.repositories.CustomerRepository;
import com.example.demo_spring_security.utils.JwtUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class LoginController {
    @Autowired
    CustomerRepository _customerRepository;
    @Autowired
    private JwtUtils jwtUtils;
    @Autowired
    AuthenticationManager _authenticationManager;
    @Autowired
    private PasswordEncoder _passwordEncoder;

    @PostMapping("/register")
    public ResponseEntity<String> registerUser(@RequestBody Customer customer){
        Customer customer1 = null;
        ResponseEntity response = null;
        try {
            String hashPwd = _passwordEncoder.encode(customer.getPwd());
            customer.setPwd(hashPwd);
            customer1 = _customerRepository.save(customer);
            if(customer1.getId()>0)
                response = ResponseEntity.status(HttpStatus.CREATED).body("User created");
        }catch (Exception e){
            response = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Une erreur est survenue " + e.getMessage());
        }
        return response;
    }

    @GetMapping("/user")
    public Customer getUserDetailsAfterLogin(Authentication authentication) throws Exception {
        List<Customer> customers = _customerRepository.findByEmail(authentication.getName());
        if(customers.size() > 0)
            return customers.get(0);
        else
            throw new Exception("User not found");
    }


    @PostMapping("/login")
    public ResponseEntity<LoginResponseDto> loginUser(@RequestBody LoginRequestDto loginRequestDTO) throws Exception {
        try {
            Authentication authentication = _authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginRequestDTO.getEmail(), loginRequestDTO.getPassword()));
            SecurityContextHolder.getContext().setAuthentication(authentication);
            String token = jwtUtils.generateJwtToken(authentication);
            return ResponseEntity.ok(LoginResponseDto.builder().token(token).build());
        }catch (Exception ex) {
            throw  ex;
        }

    }
}
