package com.example.demo_spring_security.repositories;

import com.example.demo_spring_security.entities.Account;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountRepository extends CrudRepository<Account, Long> {
    Account findByCustomerId(int customerId);
}
