package com.example.demo_spring_security.repositories;

import com.example.demo_spring_security.entities.Cards;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CardsRepository extends CrudRepository<Cards, Integer> {
    List<Cards> findByCustomerId(int customerId);
}
